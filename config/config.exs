# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :dynt,
  ecto_repos: [Dynt.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :dynt, DyntWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aZddNyo4B1BJKkrnUKdm2LNryOkC/Hy3rAFXNqrAVICjG9o1OvC+HgvmeHkAMFf3",
  render_errors: [view: DyntWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Dynt.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
